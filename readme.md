[Bachelors Thesis][linkedin]
============================
![GitHub version][version_badge] [![Build status][travis_image]][travis_status]

_Undertaking a project for the University of the West of England's BSc (Hons) Software Engineering degree._

Setup
-----
To generate the PDF version of the report, run the following command:
```bash
pdflatex report.tex && biber report && pdflatex report.tex
```

License
-------
Copyright © Mr Axilus.
This project is licensed under [CC BY-NC-SA 4.0][license].

[license]: https://creativecommons.org/licenses/by-nc-sa/4.0/
[linkedin]: https://www.linkedin.com/in/mraxilus
[travis_image]: https://secure.travis-ci.org/mraxilus/bachelors-thesis.png?branch=master
[travis_status]: https://secure.travis-ci.org/mraxilus/bachelors-thesis
[version_badge]: https://badge.fury.io/gh/mraxilus%2Fbachelors-thesis.svg 
