\chapter{Conclusion}
\section{Summary}
  The field of Volunteer Computing have become a well established means for the acquisition of large scale computational resources.
  Even so there are still a few potential scenarios with which this paradigm has not fully been applied.
  Allowing owners of low-cost devices to share computational power amongst themselves is one such use case.
  This research set out to create a system with which the volunteers also becomes a benefactor in terms of the gain of computational power.
  One of the major objectives was to do this using low-cost devices such as the Raspberry Pi so that it could be a feasible option for students,
    startups,
    individuals,
    and research projects.

  A literature review was undertaken which explored the development of relevant fields of computing,
    namely Volunteer Computing,
    Peer-to-Peer,
    and MapReduce.
  These terms where clearly defined in the context of this system,
    and the history and development of each area was analysed.
  Research most relevant to the system was discussed which helped refine the direction that the system would head towards.
  Towards the end of Chapter 2 an evaluation of similar software was undertaken,
    and reasons for why they do not fulfill the outcomes of the system were provided.

  After enough knowledge and context of the area with which the system would reside had been accumulated,
    the system domain was clearly defined.
  The stakeholders of the system were defined to be developers,
    who create and distribute work units,
    volunteers,
    which contribute to the system by donating their idle processing capacity,
    and bystanders,
    who are co-users of the devices running the system.
  Once defined,
    the features and requirements of each of the system's stakeholders were then captured via the creation of user story cards.
  The story cards were then prioritized in an effort to ensure the core functionality of the system would be constructed before any additional features.

  Next,
    the higher level design of the system was presented which laid out the simplified version of the MapReduce algorithm to be used to process work in the system.
  After which,
    decisions were made as to the hardware to be used for system testing.
  A few concepts where defined such as peer awareness,
    and the serialization and transmission of work units to the devices of volunteers.
  The design chapter then expanded upon the user stories to create specific test scenarios that would determine whether or not the system met its requirements.

  Chapter 5 covered the details of the architecture of how the system was actually implemented.
  It goes on to cover the hardware components of the system,
    and outlines their installation and total cost. 
  The implementation section then describes the system's message protocol,
    and the communication and distribution of work units.
  Also,
    the system is evaluated against the test scenarios outlined in the design chapter.

  Lastly,
    Chapter 6 starts off by comparing the design of the system to its actual implementation,
    all in an effort to evaluate what the system has achieved.
  The chapter then moves on to discuss the security implications of the potential usage of the system,
    and proposes a possible solution,
    that is implementing a parser for a subset of the python language.
  Finally,
    a few different limitations of the system are analysed.
  

\section{Learnings}
  Much has been learned about the problem space throughout the course of this investigation.
  Generating new ideas in the area computing is not an easy task.
  Originally there was not any single direction that this study was heading.
  Although area of Volunteer Computing has been fairly well established,
    software such as BOINC remains unheard of by most until they start venturing into this area.
  One of the major takeaways from this research would be that Volunteer Computing works best with volunteers,
    and currently,
    majority of the potential volunteers remain unaware of the existence of this paradigm.
  Knowledge of the benefits of Volunteer Computing systems needs to be distributed across as wide as a community as possible.

  Even though the field of Volunteer Computing has been saturated with scientific projects of varying kinds,
    there still remains opportunities for smaller projects to take advantage of this paradigm.
  Harnessing the power of Peer-to-Peer technologies,
    everyday developers could be able to share their computational processing,
    increasing each and every one of their developmental capabilities. 

  Researchers have recently published work showing the promise of MapReduce in a Volunteer Computing setting,
    however many components of the paradigm,
    such as the verification of results,
    have yet to be explored in this context.
  One thing that would need to be developed in order to enable ad hoc benefactors of a Volunteer Computing system,
    would be the creation of a safe means of perform the execution of potentially hostile source code.

  In terms of this investigation's developmental process,
    incorporating Agile principles in a research project enabled the elicitation of clear and concise requirements,
    without choking research and development.
  It allowed the system to be built from its core functionality outwards,
    and made the entire process a pleasant experience.
  The Agile methodology really does lend itself towards iterative projects.

  There was a point in the implementation of the system when progress was thought to have hit a brick wall.
  The initial design did not account for the specific socket connection method with which connections with peers where to be made.
  Using a Transmission Control Protocol socket server originally worked for workers in the system,
    however it was unclear as to how the master would receive responses without blocking and waiting for responses.

  Python generators looked as though it would be a promising approach to solving this problem,
    however they are not asynchronous,
    but rather,
    lazily evaluated.
  The solution in the end was to create an socket server that runs forever,
    in a separate thread,
    receiving responses from any source.
  Meanwhile the main thread sleeps,
    only awaking when the amount of results received is equivalent to the work units sent out to workers.
  
  In terms of an evaluation of the choices made throughout this study,
    surprisingly,
    most choices were more than satisfactory.
  Python proved to be a suitable language for both evaluation and communication purposes.
  Raspberry Pis,
    when configured with Raspbian,
    where easy to configure and setup.
  Also,
    MapReduce is a very clear model which was a pleasure to implement as a part of the system.

\section{Future Work}
  Improvements to the work completed in the project could potentially be produced.
  The system could be developed further in multiple areas such as peer indexing,
    code evaluation,
    and providing a points based system to encourage friendly amongst volunteers.
  The basis for all of these areas currently exists in the system,
    however additional design work would need to be undertaken.

  The peer indexing as implemented is fairly naive.
  Peers may change address,
    become error prone,
    or fail to communicate entirely.
  Currently the peer index does no take these sort of concerns into account,
    but instead randomly choose peers to distribute work to.
  Consider the implementation of weighting system.
  Each destination in the index would also be combined with a measure of fitness that decays over time.
  This metric would take into account,
    how many successful work units have been completed,
    the frequency of refused or timed out connections,
    and the amount of erroneous results send back.
  The time based corrosion of this fitness value would also cover nodes that either move address,
    or go down completely.

  Of course,
    tracking this sort of data without having it get in the way of processing,
    would require some sort of housekeeping step run on specified intervals.
  The master would have to scrub stale peers from the index to prevent overgrowth,
    and ensure that enough peers are kept as to maintain the benefit of having large scale distributions.
  The fitness values would vary for each node in the system,
    thus the master would also have to constantly maintain an acceptable fitness threshold,
    to enable more optimal peer selections to take place.

  When considering the current state of code evaluation in the system,
    significant improvements to,
    not only its security,
    but its syntactic capabilities as well.
  As mentioned previously,
    much of the potential security vulnerabilities could be avoided by writing a parser for a subset of the Python programming language.
  This would most likely be done through a library such as $ast$,
    or abstract syntax trees.
  Having a map and reduce function parser would enable greater functional capabilities,
    as functions would not have to be white listed one by one as deemed safe.

  In addition,
    one of the most greatest ways to entice individuals to become long term volunteers,
    would be to implement a credits or points based recognition system.
  This system would have ties with the peer fitness system mentioned earlier,
    as the major factors for crediting a volunteer's devices would be how other peer devices rate its fitness.
  Another possibility would be to implement a metric into the system,
    which tracks how quickly a peer produces results for work units assigned to it.
  This would allow volunteers to be attributed credit based on how temporally reliable their devices are.
  Most of these volunteer statistics would most likely reside on a central web service.
  A service which would provide a simple an easy way to compare your network contributions to other users on the network.

  There are countless additions that could be made to expand upon the work accomplished in this investigation.
  Currently the system has accomplished what it had set out to do,
    and functions as expected.
  This concept could be pursued further in the future,
    and would greatly benefit not only large scientific institutions,
    but the global developer economy.

  \begin{quote}
    ``My dream is to give everyday developers,
      free access to large amounts of parallel computational power,
      especially if they only have access to low-cost computing devices.
    The completion of this system demonstrates that one way of achieving this goal,
      is to get these developers to take the devices they already own,
      and help each other.'' --- Sinclair-Emmanuel Smith
  \end{quote}

  
