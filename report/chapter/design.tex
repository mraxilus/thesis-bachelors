\chapter{Design}
\section{Architecture}
  The software design and overall architecture of the system shall now be explored more thoroughly.
  The system shall be composed of multiple devices which act as peers,
    or nodes,
    in the system's network.
  Every peer should run duplicate instances of the same software,
    such that each peer can act as both a master and worker for jobs being run throughout the system's network.
  To understand how the different roles of the system interact with each other the MapReduce abstraction needs to be examined in more detail.
 
  %\subsection{MapReduce}
    MapReduce requires that for a single job there exists one master,
      or distributer of work units,
      and at least one worker,
      or the executor of distributed units of work.
    Upon the retrieval of a newly submitted job, 
     it is the master's responsibility to gather the source data,
     and divide it up into key-value pairs which get assigned to each worker as map tasks.
    For each task,
      the workers must retrieve,
      from the master,
      the map function along with a key-value pair divided from the source data.

    Once all of the key-value pairs have been assigned to workers along with the map function to process them,
      the master must wait for the results.
    The results passed back from the workers take the form of intermediate key-value pairs.
    As many of the results` intermediate pairs contain values for the same key,
      the reduce function is needed to translate the intermediate results into the final results.
    
    It is at this point that the master,
      again,
      divides up the key-value pairs and assigns them to workers.
    The difference this time is that the master must first coalesce the intermediate results so that there are no duplicate keys.
    This is accomplished by combining the values of duplicate keys into a single list.
    The intermediary data structure is now a associative array with unique keys,
      and a list of values obtained from the map process.

    \begin{algorithm}
      \caption{Generalized type definitions of the distributive stages of the MapReduce process~\autocite{article:mrsdplc}.}
      \label{alg:gtddsmrp}
      \begin{algorithmic}[1]
        \State \Call{map\hskip2em}{k1, v1}\hskip3.25em$\rightarrow$\hskip1em$list$(k2, v2)
        \State \Call{reduce\hskip0.5em}{k2, $list$(v2)}\hskip1em$\rightarrow$\hskip1em$list$(k2, v3)
      \end{algorithmic}
    \end{algorithm}

    These concepts can be more easily understood by examining the type definitions in Algorithm \ref{alg:gtddsmrp},
      and describing the flow of data without the consideration of masters and workers.
    The source data is an associative array which holds multiple keys $k1$ and values $v1$.
    These key-value pairs are then individually passed into separate map functions.
    Each instance of the map function processes its $k1$ and $v1$ and produces a single intermediate result,
      which is a single associative mapping of $k2 \rightarrow v2$.
    Combining all the intermediary results from the map functions,
      there will then be a list of $k2 \rightarrow v2$ associations,
      which the type definition shows as\hskip0.5em$list(k2, v2)$.

    After the generation of intermediary results completes theres a subtle conversion,
      one which is not clearly shown in the type definition.
    Since there may exist many duplicate keys,
      they must be combined without the loss of their respective values.
    This is to be done by creating a new dictionary which has unique keys $k2$,
      and values which are a list of the amalgamated $v2$ values,
      which results in $k2 \rightarrow list(v2)$.
    Not by chance,
      separated,
      these new mappings match the parameters required by a reduce function.

      Taking the intermediate keys $k2$ with their coalesced values $list(v2)$,
        and pass them as arguments to as many reduce functions as there are keys,
        the final results are then produced.
        These results take the form of $list(k2, v3)$,
          where $v3$ is produced from the list of values $v2$.

  %\subsection{Overview}


\section{Infrastructure}
  There are a few hardware needs that must be fulfilled in order to test the system's implementation.
  There's the specific low-cost device to use,
    the router that connects them all together,
    and either a monitor mouse and keyboard or a computer to be used for remote sessions.
  Of the latter,
    remote sessions would be the far more convenient option,
    as hot swapping the peripherals between devices would place restrictions on how many devices can be monitored at any given time,
    that is to say, one.
  As most operating systems,
    more specifically the one to be used for testing,
    support multiple terminals,
    multiple secure shells can be monitored simultaneously.

  Many individuals own various low costs devices which they use for media centres,
    file servers,
    and various other purposes.
  Since there is no single device that is globally preferable,
    a decision must be made.
  Take two of the most popular hardware solutions for embedded systems development,
    the BeagleBone Black and the Raspberry Pi.
  Both have their merits however only one is needed for the demonstrational purposes of this system.

  \begin{figure}[h]
    \centering
    \includegraphics[width=0.8\textwidth]{image/beaglebone_black_vs_raspberry_pi@986x657.jpg}
    \label{figure:bbbrp}
    \caption[BeagleBone Black versus the Raspberry Pi]{
      Exhibited in the image above is a BeagleBone Black displayed adjacent to a Raspberry Pi~\autocite{website:bbbrp}.
    }
  \end{figure}

  Comparing the specifications of the two devices,
    provided in Table~\ref{table:ctlcda},
    a choice can be made.
  The price of the Raspberry Pi is marginally cheaper than that of the BeagleBone,
    which makes it a more enticing option for volunteers on a budget,
    however as the Model B has no built-in storage capabilities.
  This difference could be offset by the price of SD cards,
    which for 4GB is approximately \$10~\autocite{website:sdcrpprw}.

  The BeagleBone is superior in terms of processing capability,
    but there are also factors not captured in their specifications worth considering.
  Raspberry Pis have gathered a greater community,
    as the BeagleBone is tailored to more embedded applications rather than multimedia projects.
  Many individuals run media servers on their Raspberry Pis due to its preferable graphics processing capabilities.
  This means that Raspberry Pis could prove to be a better market for this system to cater towards,
    as media centres tend to only be used in short bursts rather than continuously.

  \begin{table}[h]
    \centering
    \caption{Comparison of two low-cost device alternatives, the BeagleBone Black and the Raspberry Pi Model B~\autocite{website:hcrprpbbb}.}
    \label{table:ctlcda}
    \begin{tabular}{p{0.3\textwidth}p{0.3\textwidth}p{0.3\textwidth}}
      \toprule
      Specification & BeagleBone Black & Raspberry Pi Model B \\
      \midrule
      Price & \$45 & \$35 \\
      Processor & 1GHz & 750MHz \\
      Memory & 512MB @ 400MHz & 512MB @ 400MHz\\
      Storage & 2GB, MicroSD & SD \\
      Peripherals & 1 USB, 1 Mini-USB, \mbox{1 Ethernet} & 2 USB, 1 Micro-USB, 1 Ethernet \\
      Operating Systems & Angstrom, Ubuntu, ArchLinux, etc. & Raspbian, ArchLinux, Fedora, etc. \\
      \bottomrule
    \end{tabular}
  \end{table}

  For the reasons mentioned previously and from personal experience with it,
    the Raspberry Pi is a better fit for this research,
    and shall be used for the individual nodes of the system.
  Properly test the operation of one master and multiple workers at least three Raspberry Pi devices will be needed.
  Each of these device shall be powered via a mains outlet and networked with its peers via Ethernet connections to a router.

  The router to be used is the D-Link DIR-605L for it is simplistic,
    and under a price point of \$17.99 refurbished~\autocite{website:dlwnmhcaebr},
    it is extremely affordable. 
  In addition,
    the computing device to be used for the initiation of remote sessions shall be a System76 Bonobo Extreme,
    simply because it is pre-owned,
    however most computers running a variant of the Linux operating system should be capable of replicating the functionality needed.
 
  

\section{Communication and Distribution}
  The system's logistics and hardware have been covered,
    but the specific means of work transportation has yet to be defined.
  Some sort of data serialization method is needed as both the source data,
    and the developer's map and reduce functions need to be distributed amongst nodes in the system.
  There exists information that each node needs to interact with one another, 
    such as the locations of its peers.

  \subsection{Peer Awareness}
    In most Peer-to-Peer systems a tracker is implemented which allows new nodes to locate its peers.
    The disadvantage to this approach is that individuals must continually host and maintain tracking servers in order for the network to function.
    For the purposes of creating a low-cost,
      ad hoc alternative to other distributed systems,
      the costs of tracker maintenance would be far less than an ideal situation.

    Rather than implementing a tracker system with which some peers become monitors of the locations of other peers,
      this system shall make each node responsible for tracking its own potential workers.
    This shall be accomplished by making one requirement,
      each node in the system,
      upon installation,
      should be given the locations of at least one other peer.
    Once started,
      a node should keep track of the locations of all nodes that communicate with it,
      both known and unknown,
      and pass their index of locations along to those nodes.
    In effect,
      what should happen is that as more nodes join the system,
      the existing nodes build up larger and larger indexes of operating peers.

    There are a few problems that arise with the self-indexing approach,
      and their solutions shall be described.
    The first problem is that indexes may grow too large to be transmitted efficiently.
    This growth issue can be stunted by placing a maximum restriction on how many locations a node can share at one time.
    If a node happened to have a location index larger than the maximum restriction they simply randomly choose locations until that limit is reached,
      and distribute that subset of locations to its peer.
    The more interactions that take place in the system,
      the greater the propagation of peer locations.

    Another major issue with self-indexing is that nodes may be extremely distant from each other,
      physically,
      and the connection between peers may be intermittent or unreliable.
    To counter this a distance metric will need to be created for each indexed location.
    Before a node chooses to assign work to a peer,
      it should first test the connection to ensure that it meets an acceptable threshold.

  \subsection{Data Serialization \& Transmission}
    Google's protocol buffers has become a popular method of data serialization.
    It allows the clear an concise definition of a protocol to which objects can be easily created and serialized by one node,
      transported across the network via sockets,
      and parsed back into object form by the receiving peer.
    Using protocol buffers should vastly simplify the network design.

    Protocol buffers provides support for three languages,
      Java,
      Python,
      and C++.
    Python lends itself more towards this system when compared to Java or C++ as it is a dynamic language.
    Dynamic languages allow the modification and evaluation of code at runtime,
      that comes in the form of strings.
    This feature is essential for the delivery of the source for the map and reduce functions from a master to its workers using protocol buffers,
      and thus Python is the language to be used.

    Python also provides code inspection,
      that is,
      the ability for a program to examine its own source code.
    This means that developers in the system can define their map and reduce functions in the normal syntax of the python language,
      rather than in some domain specific way.
    The system can then inpsect the source of their functions to convert them into strings.
    These string representations of the devepor's functions can then be package in a protocol buffers message.
    On the other side of the peer communication,
      the program can evaluate the string representation of the developer's map and reduce functions locally.

    Figure~\ref{fig:ppbcsd} provides a diagram of the protocol buffers schema to be used in the system.
    Starting with the Packet message,
      its responsibility is to encompass all fields and messages into a single message.
    This is done so that each peer only has to parse a single object with which,
      they can retrieve and sub-messages from.

    \begin{figure}[h]
      \centering
      \includegraphics[width=\textwidth]{image/protobuf_diagram_design@1200x1050.png}
      \caption[Proposed protocol buffers communication schema diagram.]{
        Shown here is the proposed protocol buffers communication schema diagram for the distribution of work units amongst peers.
      }
      \label{fig:ppbcsd}
    \end{figure}

    In the Packet message,
      the source is the location of the device from which the packet originates.
    The peers container contains a subset of the source's indexed peers,
      this allows for propagation of peer addresses.
    Finally,
      the task field contains the work for the peer to execute if sent from the master,
      or the result of the assigned work if sent from a worker.

    The Task message provides all the information necessary about a particular work unit.
    The job field is to be a universally unique identifier which identifies a particular job.
    If peers were to send packets to the wrong master server,
      the master server should be able to differentiate whether or not received work units belong to the currently submitted job.
    The key-value pair for a particular task are divided into separate fields to simplify the conversion from strings to Python objects,
      this is to be done with generalised conversion functions.

    Code provided in the function field should be consistent with the specified Type enumeration.
    The Type enumeration denotes whether a given Task message is a map work unit,
      or one that is a reduce.
    A Task with a type of MAP should have the source of the developer's map function in the function field,
      and the same goes for a Task with a type of REDUCE.

\section{Testing}
  Expanding upon the user stories developed in the Requirements chapter,
    the following test scenarios outline specific work flows which can determine whether or not the finish system satisfies the original requirements.
  Following in the footsteps of user stories,
    scenarios in the Agile process often come in a structured form as well.
  The universal template to be used in this instance is,
    ``Given [state], When [event], Then [result].''
  This format allows each test case scenario to be clearly defined as transitions between states,
    whilst taking into account the perspective of the system's stakeholders.

  \begin{table}[h]
    \centering
    \caption{Scenario --- Developer distributes work using MapReduce.}
    \begin{tabular}{p{0.8\textwidth}}
      \toprule
      \textbf{Given} that map and reduce functions are well defined. \\

      \hspace{1.5em} And the system's software library is imported \\

      \hspace{1.5em} And the dataset has been constructed correctly, \\
      \midrule
      \textbf{When} a submission is made with the dataset and functions. \\
      \midrule
      \textbf{Then} any available volunteers get assigned tasks. \\
      \bottomrule
    \end{tabular}
  \end{table}

  When a developers intend to distribute their computations using the system,
    they must first define their map and reduce functions,
    adhering to the outlined protocol.
  Not only that,
    but their source data must be in an appropriate form,
    that is to say a dictionary of key-value pairs.
  These key-value pairs must be separately processable by the developers map function,
    and all of the mappings emitted from the map function,
    must also be processable by the reduce function,
    which then produces the final results to be returned to the developer.
  Once these prerequisites are satisfied and a job has been submitted,
    the system should distribute the work to any available volunteering systems.
  

  \begin{table}[h]
    \centering
    \caption{Scenario --- Volunteer donates spare processing capacity.}
    \label{table:svdspc}
    \begin{tabular}{p{0.8\textwidth}}
      \toprule
      \textbf{Given} that Python is installed \\

      \hspace{1.5em} And the system's script is downloaded,\\
      \midrule
      \textbf{When} the script is executed from the terminal \\

      \hspace{1.5em} And an optional peer address is provided, \\
      \midrule
      \textbf{Then} the system services work packets from any peer. \\
      \bottomrule
    \end{tabular}
  \end{table}

  Referring to the scenario in Table~\ref{table:svdspc},
    in order for a volunteer to become a donor of their spare processing capabilities,
    they must first have Python installed on their device.
  In addition,
    volunteers must have the system's executable script.
  For the volunteered devices on the network to learn about each other,
   when running a worker providing a single peer is necessary so that they can share each others address location.
  
  \begin{table}[h]
    \centering
    \caption{Scenario --- Developer imports the system.}
    \begin{tabular}{p{0.8\textwidth}}
      \toprule
      \textbf{Given} that Python is installed
      
      \hspace{1.5em} And the system's software library is downloaded,\\
      \midrule
      \textbf{When} the library is imported via Python's $import$ built-in, \\
      \midrule
      \textbf{Then} Python should import the system without errors,
      
      \hspace{1.5em} And the job submission function should be available.\\
      \bottomrule
    \end{tabular}
  \end{table}

  Allowing the developer to import the system as they would with any other Python package,
    allows the system to have virtually no configuration needed.
  When compared to the setup process of other Volunteer Computing systems such as BOINC,
    this would be an extremely enticing alternative for developers who do not have time to invest in gaining specific domain knowledge.
  Having a straightforward development process allows the developer to focus on what really matters,
    their processing algorithms.
  This is essential in improving the attrition of new developers in this field,
    as currently the advantages of the Volunteer Computing paradigm are lost to the average developer.

  \begin{table}[h]
    \centering
    \caption{Scenario --- Developer connects to existing network.}
    \label{table:sdcen}
    \begin{tabular}{p{0.8\textwidth}}
      \toprule
      \textbf{Given} that existing volunteers are running the system. \\

      \hspace{1.5em} And the system's software library is imported,\\
      \midrule
      \textbf{When} a job is submitted to a single volunteer. \\
      \midrule
      \textbf{Then} existing volunteers should become known. \\
      \bottomrule
    \end{tabular}
  \end{table}

  Table~\ref{table:sdcen} outlines the scenario with which a developer connects to an existing network of peers,
    after all,
    this is the entire essence of this investigation.
  Volunteers must already be donating their devices as workers in the network,
    which then allows developers to improve the computational capacity.
  By connecting to an existing network,
    the master can retrieve an index from a single node in the system.
  Subsequently,
    the index of peers for the job can be grown further from each peer which is to be assigned work.
  This allows the system to only require the developer to know the address of a single existing volunteered device.

  \begin{table}[h]
    \centering
    \caption{Scenario --- Volunteer installs and runs the system.}
    \begin{tabular}{p{0.8\textwidth}}
      \toprule
      \textbf{Given} that a individual has a low-cost device to volunteer \\

      \hspace{1.5em} And Python is installed on it \\

      \hspace{1.5em} And the device access to networks of other peers, \\
      \midrule
      \textbf{When} they desire to install and run the system, \\
      \midrule
      \textbf{Then} they just have to download and run a script. \\
      \bottomrule
    \end{tabular}
  \end{table}

  One of the intentions of this investigation is to ensure that volunteers without domain knowledge,
    or even technical experience,
    would be able to install and run the system without significant barriers.
  This would include things such as mandatory configuration of the system,
    source code compilations,
    and custom installations of dependencies.
  Volunteers should be able to install the system with minimal effort.
  The process should be as simple as downleading and running a script.

  Some volunteers may require a monitoring system to track the amount of work being assigned to their devices.
  Adding tracking to workers would enable these volunteers to ascertain the contributions they are making to the system.
  For these cases,
    the system shall log identifying information such as the type of work recieved,
    that is whether or not it is a map or reduce task,
    and the key-value pair attached to it.
  Additional information could potentially be logged such as the address of the device that assigned the work,
    and the function's source code,
    however for the sake of anonymity this may not be necessary.

  \begin{table}[h]
    \centering
    \caption{Scenario --- Volunteer tracks delegated work.}
    \begin{tabular}{p{0.8\textwidth}}
      \toprule
      \textbf{Given} that an individual is already volunteering a device, \\
      \midrule
      \textbf{When} new work gets assigned to their volunteered device, \\
      \midrule
      \textbf{Then} relevant identifying information should be logged. \\
      \bottomrule
    \end{tabular}
  \end{table}

  Finnally, 
    the scenario in Table~\ref{table:sbruoa} covers bystanders affected by the operation of this system.
  Many times low-cost devices are utilised by multiple individuals,
    and thus when the device is executing a task,
    the system needs to account for changes in processing usage.
  If a process contesting for space exists,
    the system needs to be able to detect it,
    and give it free access to the devices resources by avoiding any additional work assignments.

  \begin{table}[h]
    \centering
    \caption{Scenario --- Bystander requires usage of other applications.}
    \label{table:sbruoa}
    \begin{tabular}{p{0.8\textwidth}}
      \toprule
      \textbf{Given} other users require the use of volunteered devices \\

      \hspace{1.5em} And assigned work has significant processing overhead, \\

      \midrule
      \textbf{When} bystanders attempt to utilise other applications, \\
      \midrule
      \textbf{Then} the system should avoid accepting additional work. \\
      \bottomrule
    \end{tabular}
  \end{table}
