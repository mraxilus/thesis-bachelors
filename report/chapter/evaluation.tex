\chapter{Evaluation}
\section{Implementation Analysis}
  Surprisingly,
    the implementation remained fairly consistent with the original design.
  Any changes that were made were due to issues encountered in the development of a prototype.
  Python proved to be a good choice as its dynamic capabilities allowed for the inspection and evaluation of map and reduce functions.
  In addition,
    Raspberry Pis provided a stable and cost efficient platform with which to test the system.

  In terms of the possibilities made available by the MapReduce paradigm,
    the system still remains fairly restricted relatively to what was originally intended due to the security concerns that will be explored later in the chapter.
  As it stands the only white listed functions are those used specifically by the test examples.
  In order to provide greater flexibility to developers,
    a complete list of acceptable functions would need to be developed and evaluated.

  There are a few components of the original requirements that were not fully implemented due to time constraints.
  Despite this ideas and concepts where explored which have provided enough knowledge for their completion.
  Overall,
    the implementation of this system,
    relative to its design,
    has resolved itself to be a successful investigation into the feasibility of Peer-to-Peer Volunteer Computing systems.


\section{Security Implications}
  Members of a Peer-to-Peer system may not always be trustworthy.
  There are a few security implications inherent in the design of this and similar systems that must be considered.
  The two most prominent issues cover that of running unknowable code on a network of the devices of individuals.
  If every developer in the network were to be trusted this would not prove to be an issue,
    however the following sections illustrate how in reality,
    this is not the case.

  \subsection{Code Injection}
  \label{section:ci}
    Being a dynamic programming lanugage,
      Python allows programmers to evaluate statements and expressions,
      at runtime,
      from a string.
    This accomplished via the built-in functions $eval$ and $exec$,
      which evaluate expressions and statements respectively.
    Red flags are risen whenever the concept of executing user code.
    As mentioned previously,
      in a Peer-to-Peer system peers inherently cannot be trusted.
    To execute the unknown code of untrustable peers is a major security flaw.
    For example,
      the top rated web application vulnerability of 2010 was code injection,
      or rather,
      exploits of a system's interpreter via text inputs~\autocite{website:ti}.


    \begin{algorithm}
      \caption{Evaluation of a malicious strings in Python~\autocite{website:erd}.}
      \label{alg:emsp}
      \lstset{gobble=6,
              language=Python,
              showstringspaces=false}
      \begin{lstlisting}
        # with globals.
        eval("os.system('clear')")  

        # without globals.
        eval("__import__('os').system('clear')", {})  
      \end{lstlisting}
    \end{algorithm}

    There has been much discussion on the safety of Python's $exec$/$eval$ commands.
    Algorithm~\ref{alg:emsp} shows examples of this.
    These commands used on their own provide the evaluated source code access the currently active global and local variables,
      $globals$ and $locals$ respectively.
    The first evaluation is an example of a user modifying the program's execution environment via the $os$ module.
    To avoid this,
      the second evaluation provides a substitute for the global scope which is an empty dictionary,
      however the module can still be imported via Pythons built-in $\_\_import\_\_$ method.
    Clearing the global scope is not enough avoid malicious code injection.

    One final modification,
      deemed safe by some,
      is to remove access to pythons built-ins altogether by providing the evaluation method with a gloabal scope that explicitly sets $\_\_builtins\_\_$ to $None$.
    Although a large majority of potential holes have been filled,
      some malicious users are extremely resourceful as seen in Aglorithm~\ref{alg:emspbla}.
    This particular piece of source causes the program evaluating the statements to encounter a segmentation fault.
    In the context of this study,
      that could mean failure of any node that receives a malicious packet.

    This is not a losing battle however as programmers could refuse the execution of any statements that contain double underscores,
      which would counter every work around that has been encountered thus far.
    This is believed to be a safe way to avoid injection attacks from mischievous users,
      however it would be hard to prove it.


    \begin{algorithm}
      \caption{Evaluation of a malicious statements in Python with built-ins no longer available~\autocite{website:erd}.}
      \label{alg:emspbla}
      \lstset{gobble=6,
              language=Python,
              showstringspaces=false}
      \begin{lstlisting}
        statements = """
        (lambda fc=(
          lambda n: [
            c for c in 
              ().__class__.__bases__[0].__subclasses__() 
              if c.__name__ == n
            ][0]
          ):
          fc("function")(
            fc("code")(
              0,0,0,0,"SEGFAULT",(),(),(),"","",0,""
            ), {}
          )()
        )()
        """
        eval(statements, {'__builtins__': {}})
      \end{lstlisting}
    \end{algorithm}

    Ensuring the safety of systems on the network is not an easy task.
    It is feasible that users bent on harming the system in some way will develop methods to do so.
    The only true solution to the problem would be to utilise a library such as Python's $ast$ to write a restricted parser,
      which rather than blacklisting methods of injections,
      would whitelist code deemed acceptable.

  \subsection{Supporting Illegal Activities}
    Another potential security issue is that volunteers may not fully have control over what they are supporting.
    Since the system allows job submissions from any source,
      some of the work being assigned to volunteers may not be entirely legal.
    Individuals distributing controversial work into the system would effectively be gaining processing power from innocent volunteers.
    These volunteers would then be giving up their idle processing capacity to aid in illegal activities.

    One can suppose that a function checking system could be implemented where the desired goal of the code could be analysed by the nodes performing work.
    There are a few complications with that supposition.
    The MapReduce abstraction does not always make things easier,
      it actually makes it more difficult to ascertain the purpose of the work being performed,
      as each node will only receive small fragments of the total key-value mappings.
    Not only that,
      but each worker in the system may only receive only map assignments,
      or only reduce assignments which provides only half the picture.

    A volunteer might be able to determine where their device's work is coming from via the indexed locations,
      however the malicious developer could be using a proxy-like system to cover their tracks.
    Because of the ad hoc nature of work assignment the system,
      it makes it extremely difficult keep track of a specific job.

\section{Limitations}
  The system as it stands is fairly limited in some regards.
  Python was the language chosen to build the system upon,
    and in order to utilise this system developers must be able to program in the language.
  This puts restrictions on the amount of developer that can take advantage of such a platform.
  As an example,
    an existing project written in Java might exist that's looking to perform parallel computation.
  In order to take advantage of our system they would have to either write an external python script,
    or port their system to python.
  Whilst the former option may seem preferable it would fragment their system,
    and make it harder to understand and maintain.

  MapReduce was originally designed to work with distributed file systems such as Google File System.
  This system currently distributes work data over the network with each Task message.
  For smaller implementations of the MapReduce algorithm this is not a significant problem,
    as we have seen with the word count example,
    however for implementations of the algorithm with extremely large datasets this will prove to be a significant problem.

  Finally,
    due to the security implications discussed in Section~\ref{section:ci},
    the system has a severely limited set of available built-in functions.
  Currently the only white listed functions are those required by the example MapReduce algorithms used to test the system.
  This may prove very frustrating for programmers who are used to always having access to functions such as $abs$,
    $map$,
    $sorted$,
    etc.
  A careful selection of safe functions would need to be made available to avoid this,
    however some users will remain frustrated when a function they require is not made accessible.
  
  
