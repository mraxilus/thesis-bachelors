\chapter{Implementation}
\section{Architecture}
  The implementation of the system closely mirrors the design.
  There are a few alterations and additions,
    however both what they are and their rationale will be pointed out where relevant.
  Most importantly,
    the core components of the system are the master server,
    the worker server,
    and the $submit$ function.
  The details of their internal workings will be covered in Section~\ref{section:icad},
    Communication and Distribution,
    as this section shall focus on the overall work flow and architecture.

  The system is comprised of two main files,
    $functions.py$ and another file named $communication.py$,
    where $communication.py$ holds the source for the system's server handlers,
    argument parsing,
    and submission function,
    and $functions.py$ contains functions that are commonly reused.
  The other significant files include $communication.proto$ and $communication\_pb2.py$,
    which is the protocol buffers schema and the generated Python bindings respectively.

  The communication file is constructed in such a way,
    that when executed from the terminal it acts as a worker server daemon,
    but when imported by another Python script,
    it allows for the submission of jobs via the creation of a master server.
  This was an important feature to have as it allows both developers,
    and volunteers,
    to refer to the exact same Python script.
  Defined in this file are the index used both for masters and workers,
    and the white listed functions for user defined map and reduce,
  This simplifies the system as a whole and avoids confusion.

  The functions file contains helper functions which enable the creation of Packets and Tasks,
    the conversion of functions to strings,
    the logging of assigned and distributed tasks,
    and Packet transmission utilities.
  Ultimately the goal of this file was to ensure that there exists separation of concerns in the system,
    as the majority of the functions provided by the functions file,
    where developed outside of the original prototype as experiments.

  The only dependency of this system,
    other than the standard python libraries,
    is the Python binding for the protocol buffers.
  This was an intentional restriction placed upon the development of this system,
    as it keeps the installation process fairly clear and well defined.
  On popular Linux distributions such as Ubuntu and Raspbian,
    the protocol buffers library is provided within their built-in package management systems.
    
  As an example,
    consider a developer who desires to count the occurrences of words in a large dataset of texts,
    split up into multiple files.
  In order to utilise the system,
    a developer must first create the key-value pair mappings of the names of the files to their content,
    that is,
    $file \rightarrow contents$.
  Alogrithm~\ref{alg:cmrdsm} demonstrates how this can be accomplished.
  Given a list of files,
    $files$,
    and a function,
    $contents$ which returns the string representation of the file,
    create a mapping of $file \rightarrow contents$ for each file.

  \begin{algorithm}
    \caption{Creation of MapReduce data source mappings.}
    \label{alg:cmrdsm}
    \lstset{gobble=6,
            language=Python,
            showstringspaces=false}
    \begin{lstlisting}
      data = dict((file, contents(file) for file in files))
    \end{lstlisting}
  \end{algorithm}

  The result of the previous Algorithm would then be stored in the $data$ variable,
    which would then be separated and distributed into map tasks.
  Before this can happen the developer needs to define the map and reduce functions that will achieve the desired result.
  On a side note, 
    there was a slight parsing issue encountered when programming the conversion of functions to strings,
    for evaluational purposes.
  In effect,
    rather that returning or yielding the results,
    the results of both the map and reduce functions are expected to be assigned to a variable of the name $result$.
  

  \begin{algorithm}
    \caption{Definition of MapRedcue functions that count word occurrences.}
    \label{alg:dmrfcwo}
    \lstset{gobble=6,
            language=Python,
            showstringspaces=false}
    \begin{lstlisting}
      def mapfn(key, value):
        result = []
        for line in value.splitlines():
          for word in line.split():
          result.append((word.lower(), 1))

      def reducefn(key, value):
        result = (key, len(value))
    \end{lstlisting}
  \end{algorithm}


  The map function in Algorithm~\ref{alg:dmrfcwo} take a key and a value,
    in our case a file name and its contents,
    and emits intermediate results that are words mapped to a single occurrence.
  It accomplishes this task by splitting the file contents into lines,
    and splitting the lines into words.
  For each word in the line,
    the map function creates a new intermediate result,
    a key-value pair mapping of the particular word in question,
    and the number one.
  That is to say,
    this word occurred once.
  As the reduce function will then receive an amalgamated list of unique keys,
    and all their values,
    there will be the same amount of ones for each word as the word occurred in the original texts.
  To calculate the amount of times a particular word has occurred,
    would simply be a matter of either summing the values,
    or taking the length of the list.
  In the case of Algorithm~\ref{alg:dmrfcwo},
    the length was taken.

  The final thing the developer must do is submit the constructed data source,
    along with the map and reduce functions to a volunteer.
  Provided by the importation of the system is a submit function,
    which allows developers to do just that.
  The submission function will return a list of the results received from the completion of all of the reduce tasks.

  \begin{algorithm}
    \caption{Submission of MapReduce job into the system.}
    \label{alg:eusgtn}
    \lstset{gobble=6,
            language=Python,
            showstringspaces=false}
    \begin{lstlisting}
      results = submit(data, mapfn, reducefn, '192.168.0.1', 
                       '8899')
    \end{lstlisting}
  \end{algorithm}


\section{Infrastructure}
  An analysis of the infrastructure used to test the system will outline the methods and costs of setting up,
    and replicating the test bed.
  As the Raspberry Pi was the chosen test device,
    the components used and decisions made regarding its configuration shall be considered as well.
  The D-Link DIR-605L router only requires the connection of a power lead in order to properly function,
    so its disposition shall not be mentioned in as much detail,
    but is considered to be pre-configured.

  \begin{figure}[h]
    \centering
    \includegraphics[width=0.8\textwidth]{image/raspberry_pi_components@1494x1000.jpg}
    \caption[Components required for each Raspberry Pi device.]{
      Laid out are all the components required for each individual Raspberry Pi used in the testing of the system.
    }
    \label{fig:rpc}
  \end{figure}

  A few components where needed in order to get a functional Raspberry Pi that accessible via a secure shell.
  The device itself requires an SD card for storage,
    which will be used to host the operating system,
    which in this case is Raspbian.
  Raspbian requires at least a 4GB SD card as the image is currently larger than 2GB.
  Online web stores,
    such as Adafruit,
    sell 4GB SD cards with Raspbian pre-installed,
    however ultimately spare 32GB Micro-SDs were used with its SD adapter.

  Three Raspberry Pi were purchased in order to be able to test running the system.
  Originally a single Raspberry Pi was being powered of USB 3.0 ports from a laptop,
    however with two additional devices a new method of power was needed.
  The three devices are powered off of Micro-USB and require 5 volts of electric potential,
    and thus a 5V Micro-USB charger was acquired for each device.

  For data processing applications taking advantage of the wireless capabilities of the router is not a preferable option,
    as data processing would greatly benefit from a direct wired Ethernet connection.
  Spare Ethernet cables were installed,
    connecting the three Raspberry Pis to three of the four open ports on the router,
    the fourth port was used for a laptop in order to create ssh sessions into each of the devices.
  A fourth Raspberry Pi could have been added,
    however physically connecting the laptop was deemed preferable to a wireless connection.

  It was at this point that the overall infrastructure setup was becoming fairly unwieldy,
    and in need of organization.
  A 3D CAD of a Raspberry Pi rack support was published on Thingiverse,
    which keeps them neatly spaced and oriented~\autocite{website:rpbrsc}.
  Three of those supports were fabricated in a Replicator 2X Desktop 3D Printer.
  Once installed,
    the supports provided a much needed organizational improvement to the test bed.
  No modifications were made to the design as it was perfectly suited for this purpose.
  
  \begin{table}[h]
    \centering
    \caption{Complete list of parts used for testing the system.}
    \label{table:clputs}
    \begin{tabular}{lllr}
      \toprule
      Name & Price & Quantity & Total \\
      \midrule
      Raspberry Pi Model B & \$35 & 3 & \$105 \\
      5V Micro-USB Charger & \$12 & 3 & \$36 \\
      1m CAT6 Ethernet Cable & \$5 & 3 & \$15 \\
      D-Link DIR-605L Router & \$38 & 1 & \$38 \\
      Raspberry Pi Rack Support & \$1 & 3 & \$1 \\
      4GB Micro-SD with SD Adapter & \$10 & 3 & \$30 \\
      \midrule
      Price Per Device & &  & \$63 \\
      Price of Test Bed & &  & \$227 \\
      \bottomrule
    \end{tabular}
  \end{table}

  Compiled in Table~\ref{table:clputs} is a consolidated list of all of the hardware components used during this investigation,
    along with all of their respective prices.
  There are three Raspberry Pis in total,
    and for each they require a 5V Micro-USB charger,
    a one meter CAT6 Ethernet cable,
    a 4GB Micro-SD with an SD adapter,
    and an optional 3D fabricated rack support.
  This brings the total price per additional Raspberry Pis to \$63.
  Combined together with a D-Link DIR-605L router the total cost of implementing the hardware of the entire system is \$227.

  Consider a potential developer who is looking to purchase a low-cost device,
    such as the Raspberry Pis,
    specifically for use in this system.
  They would have the ability to take advantage of existing networks for a little over \$60,
    which is is an order of magnitude cheaper than having to implement their own cluster.
  The same relationship also stands for the simplicity of installation and configuration,
    relative to a Raspberry Pi super computer.

  As it stands,
    many individuals deem low-cost embedded devices a useful investment own their own.
  They offer an array of capabilities almost as wide as conventional servers. 
  Volunteers who already own,
    or are looking to own one of these low-cost devices,
    can also use them to contribute to the developer community at large.


  \begin{figure}[h]
    \centering
    \includegraphics[width=0.8\textwidth]{image/raspberry_pis@1333x1000.jpg}
    \caption[Raspberry Pis installed for testing and demonstration.]{
      Shown here are three Raspberry Pis physically installed in order to act as volunteered devices in the system's testing and demonstration.
    }
    \label{fig:rpc}
  \end{figure}


\section{Communication and Distribution}
\label{section:icad}

  The design of the communication and distribution of work units in the system proved to be fairly solid.
  Evidence to the fact is that the only change made to the protocol buffers specification,
    is the addition of a NONE type task,
    which allows for the distribution of peers upon connection.
  The communication between devices is best described by walking through the instantiation,
    and connection of worker and master processes.
  
  \begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{image/protobuf_diagram_implementation@1200x1050.png}
    \caption[Implemented protocol buffers communication schema diagram.]{
      Shown here is the actual protocol buffers communication schema diagram for the distribution of work units amongst peers.
      The only modification was the addition of the NONE task type enumeration.
    }
    \label{fig:ipbcsd}
  \end{figure}


  Firstly,
    a single worker must be created by executing the script without peer information.
  Additional workers may then connect to the existing worker and their address locations will be indexed by the original.
  Now a developer is able to submit a job any worker via the provided $submit$ function.
  The developer's job submission will then spawn a master thread which connects to the specified worker.
  The initial connection allows the master to retrieve the already established index of peers,
    thus acquiring the locations of all the currently existing peers without having to communicate with a tracker service.

  Once the master server has updated its index of available peers,
    it can then move onto distributing the map tasks amongst them.
  The master creates the relevant protobuf objects and distributes the tasks to random destinations,
    whilst also attaching known peers so that volunteers can update their indices as well if they are missing any known peers.

  Since workers are unaware of what types of tasks they are receiving,
    they check the type of the task so that it evaluate it in a proper fashion.
  For every task type the index is updated and its entirety sent back to the source,
    however evaluations only occur on map and reduce task types.


\section{Testing}
  Using the word count example as a basis,
    the system has fully satisfied the required features.
  Developers can easily get accustomed to the systems importation and usage.
  The system has been implemented in such a way that they can connect to existing voluteers,
    and distribute their work using the MapReduce abstraction.
  Volunteers can install and run the system,
    effectively donating the spare processing capacity,
    however the addition of a credit or points based system would improve those capabilities.
  Also,
    these volunteers can also monitor the delegation of work assigned to their devices via program output.
  Unfortunately,
    due to the implementation of the peer index being rather naive in terms of selection and reassignment,
    the needs of bystanders was not fully satisfied.
  With that being said,
    the system more than meets its aspirations in other areas.

  \begin{table}[h]
    \centering
    \caption{Completion of user stories.}
    \label{table:cus}
    \begin{tabular}{ll}
      \toprule
      User Story & Satisfied \\
      \midrule
      Developer distributes work using MapReduce. & Fully \\
      Volunteer donates spare processing capacity. & Fully \\
      Developer connects to existing network. & Fully \\
      Developer imports the system. & Fully \\
      Volunteer installs and runs the system. & Fully \\
      Volunteer tracks delegated work. & Mostly \\
      Bystander requires usage of other applications. & Partially \\
      \bottomrule
    \end{tabular}
  \end{table}
