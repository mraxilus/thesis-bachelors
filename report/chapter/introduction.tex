\chapter{Introduction}
\section{Purpose}
  The vast majority of computers are under utilized.
  Analysis was undertaken on the computer usage of a group of workstations in the Department of Computer Sciences at the University of Wisconsin.
  Over a period of 5 months,
    their analysis showed that the total utilization of the capacity of these workstations was only 30\%~\autocite{article:cahiw}. 
  These results show that there is huge potential for performing useful computation using existing hardware.
  The Condor scheduling system was developed with the hopes of maximising their department's capacity utilization by scheduling background jobs on idle workstations~\autocite{article:cahiw}.
  Many other projects have also arisen to take advantage of this discovery.

  Rather than using their own hardware,
    research initiatives such as SETI@home have attempted to acquire more computational bandwidth from the unused capacity of the computers of everyday individuals.
    SETI\hyp{}@home acquires access to these other devices via means of individuals who register computers to their service by installing an application,
    volunteering their spare processing capacity for use by these researchers.
  In 2004, running on over 1 million computers, SETI@home managed to accumulate a sustained processing rate of over 70 TeraFLOPs, over twice that of the largest conventional supercomputer at that time~\autocite{article:boincasprcs}.
  This result is unprecedented given that the cost to SETI@home for acquiring that amount of processing power is effectively free.
  Unfortunately,
    the only benefit their volunteers get, is the knowledge that they are supporting scientific progress.

  
  Volunteers themselves are not able to take advantage of this collective computational power for their own purposes.
  If massively parallel computation is required,
    many projects have to resort to implementing their own cluster of machines to perform computations.
  One example of this is Boise State University,
    who have created a cluster of machines to aid in research such as collaborative processing of wireless sensor data~\autocite{article:crpbbc}.
  Of course, this is by no means inexpensive.
  BSU's Onyx cluster is a collection of 32 computer nodes costing upwards of \$1,000 per node. 
  Thus,
  bringing the price of the entire cluster to approximately \$32,000~\autocite{article:crpbbc}.
  For any but the richest of individual volunteers,
    implementing their own cluster would be impractical, if not impossible.

  Boise State University recognises this issue and has created an alternative cluster implementation.
  Using Raspberry Pis, a low-cost computing device, they have created another 32 node cluster.
  Each node in this system costs approximately \$45~\autocite{article:crpbbc}.
  The total cost of reimplementing their cluster is now approximately \$1,500,
    less than the price of 2 nodes in their previous cluster.
  Although a major improvement in costs,
    volunteers are missing the ability to acquire large amounts of computational power with minimal to no cost to themselves,
    just as SETI@home and other @home projects have done.

  \begin{figure}[h]
    \centering
      \includegraphics[width=0.8\textwidth]{image/boise_state_raspberry_pi_cluster.jpg}
    \caption[Boise State University's 32 node Raspberry Pi cluster.]{
      32 node Raspberry Pi cluster from Boise State University's Computer Science Department~\autocite{article:crpbbc}.
    }
  \end{figure}

  Not only is low-power, 
    data intensive computing becoming an area of great interest,
    data-rich applications are becoming increasingly more mainstream~\autocite{article:iridis}.
  Research institutions are no longer the only ones in need of massive computational power.
  Due to this growth in data-rich applications,
    very soon,
    hobbyists,
    small businesses,
    hackers,
    and students,
    will all need access to perform large scale parallel computations.
  Even with the cost improvement of BSU's Raspberry Pi cluster,
    many of these individuals will still find that particular solution, 
    too impractical,
    unmanageable,
    or overly complicated.
  In some cases,
    such as those in developing countries,
    implementing BSU's cluster is still an unaffordable endeavour.
  Not only that,
    the University of Southampton's 64 node Raspberry Pi cluster achieved a throughput of 1.14 GigaFLOPs~\autocite{article:iridis}, 
    which whilst impressive,
    is only one hundred thousandth of a percent of the throughput SETI@home has achieved.
  Of course 64 versus 1 million is an unfair comparison,
    but setting up a cluster of 1 million Raspberry Pis would cost approximately \$4.5 million.
  Implementing a cluster of that magnitude is by no means feasible.
  Regardless, 
  1.14 GigaFLOPs seems especially low when you compare it to the 26 GigaFLOPs made capable by a single one of the Raspberry Pi's more parallel focused competitors, the Parallella~\autocite{website:pasfe}.

  Hobbyists, 
    students,
    small business,
    and the like, 
    will need to undertake large quantities of computation with minimal or no cost to themselves.
  Many computing devices have more than two-thirds of their capacity unutilized.
  Clustering low-cost,
    low-power devices have taken us a step further towards intensive computing for the masses,
    but we're are not quite there yet.
  An alternative method of performing these operations is required.
  This investigation aims to provide one such solution.
  Rather than having volunteers donate their spare computing capacity to established large-scale projects,
    there is also the possibility of volunteers donating their unutilized capacity to each other.

\section{Target Audience}
  The computational benefits of Volunteer Computing is not just restricted to large-scale projects.
  Those with many low-cost computing devices may be able to fully utilize their spare capacity.
  This investigation is geared towards a variety of potential users, for example:

  \begin{description}
    \item[Students,]{
      especially those in maths,
      engineering,
      and computer science,
      have dissertations and projects which sometimes require vasts amounts of computation.
      As students are not commonly known for having large budgets,
        they may not have the resources to run computations in a convenient time frame.
      They are,
        however,
        known for having access to computing devices in general,
        whether they be laptops, 
        desktops,
        arduinos, 
        netbooks, 
        and most notably,
        Raspberry Pis.
      In fact,
        78.8 percent of college students in the United States own a laptop,
        and of those 36 percent own at least 2 computers~\autocite{website:losaiwcs}.
      Most of these students would be able to use the spare capacity of these devices,
        to aid in their projects.
    }
    \item[Startups]{
        are becoming increasingly more common.
        In 2013,
          over 500,000 new business were started in the United Kingdom,
          as compared with 2012's 484,224 businesses,
          and 2011's 440,600~\autocite{website:bhrsur}.
        At the same time the amount of competition these startups have to face is staggering.
        Statistical analysis shows that 50 percent of startups fail by their 4th year~\autocite{website:sbfrbi}.
        This means that startup businesses must save on time, 
          money, 
          and effort,
          in any way they can in order to get ahead of their competition.
        In addition, one of the major causes of startup failure is eventually running out of money~\autocite{website:wsf}.
        For a startup focused on big data,
          such as gathering statistical relationships from internet content,
          spending money and time implementing one of the aforementioned clusters could potentially be a death sentence.
        Their usual options are to either implement a system to handle the processing themselves,
          or more commonly,
          pay a cloud service provider such as Amazon or Google to perform the calculations for them.
        This investigation would provide them with a cheaper,
          more attractive alternative.
    }

    \begin{figure}[h]
      \centering
        \includegraphics[width=0.8\textwidth]{image/startup_weekend_oslo.jpg}
      \caption[Entrepreneurs at the Oslo, Norway Startup Weekend.]{
        Entrepreneurs hoping to build a successful startup business at the Oslo, Norway Startup Weekend, 
          one of the many held all over the world.
        This particular event had approximately 80 attendees coming together to share ideas,
          foster relationships,
          and start businesses~\autocite{website:onsw}.
      }
    \end{figure}

    \item[Individuals,]{
      similar to students,
        sometimes also require significant quantities of computational throughput for their personal projects.
      The low-cost device hobbyist community has vastly expanded over the past few years.
      Since its first production run in 2012,
        the Raspberry Pi has sold over 2 million units~\autocite{website:tm}.
        Setting up a media centre is one of the most popular uses for this particular device~\autocite{website:10curp}.
      Assuming that these hobbyists are not perpetually consuming content on their devices,
        there would be a great deal of spare processing capacity available.
      Capacity with which they could potentially volunteer to one another,
        and utilize for themselves.
    }
    \item[Research projects]{
      such as SETI@home still remain of great importance.
      The fundamental concept of volunteered computing resources would remain the same.
      Researchers would also be able to take advantage of the computational power a project such as this would provide.
      Whilst not as effective as having a network of computers solely devoted to their specific computations,
        this investigation would provide a mutually beneficial means of accomplishing their goals.
      Again,
        this would provide them with an inexpensive alternative to setting up physical infrastructure for their projects.
        This would also enable these researchers to utilize the provided network of resources for not just one,
        but multiple of their projects.
    }
  \end{description}
  
\section{Aims and Objectives}
  Volunteer Distributed Computing systems have become a well established,
    dependable means of acquiring computational resources.
  In use by many research projects,
    individuals are able to volunteer their unutilized computing resources to provide assistance.
  Although these individuals may fully support these researching endeavours,
    they themselves may sometimes require access to such a system,
    and yet are unable to use the network for their own purposes.

  There are alternatives these volunteers can consider such as,
    implementing their own Volunteer Computing system, 
    with which they can get others to assist them,
    or they can create a low-cost computing cluster for the price of a high-end desktop computer.
  This investigation aims to create a situation in which every volunteer can also be a benefactor of the system,
    by being able to use the system's network to process their own computations.
  In order to become a viable option for volunteers of this caliber,
    this investigation:

  \begin{enumerate}
    \item{
      \textbf{Must be designed in a decentralized manner.}
      Devices should be able to join and leave a network in an ad hoc manner.
      A failure of a single device should not have catastrophic effects upon others.
      Similar to Peer-to-Peer file sharing,
        each volunteer should also be able to connect their devices to a multitude of networks,
        each network being of varying degrees of size, 
        computational throughput,
        and trustability.
    }
    \item{
      \textbf{Has to allow volunteers to run the system on low-cost devices.}
      As previously mentioned,
        having the system be able to run on low-cost devices is of utmost importance.
      Many volunteers may not have access to fully fledged desktops,
        and a low-cost device would be a much more practical investment.
      To be more specific,
        this system should be able to run on devices such as the BeagleBone,
        Raspberry Pi,
        CuBox,
        and Parallella.
    }
    \item{
      \textbf{Should be as, if not more, efficient than a low-cost cluster.}
      Since this investigation is geared towards those needing to perform large amounts of computation,
        at the very least,
        the designed system should be capable of the matching the performance achieved by known computing cluster implementations. 
    }
    \item{
      \textbf{Must ensure each device is responsible for the completion of its higher level objectives.}
      You can consider higher level objects as the computational work assigned to the nodes of the system.
      Having programs fail to complete crushes motivation,
        especially when no preliminary results or resultant states are obtained from the system.
      It also wastes tremendous amounts of time.
      Each node needs to ensure that its computational work is either completed successfully or,
        if there are unforeseen problems,
        needs to be able to preserve any relevant state information,
        thus guaranteeing that some helpful result is achieved.
    }
    \item{
      \textbf{Shall be able to support multiple computational instances.}
      This system would be much less useful if it were not able to handle simultaneous higher level objectives.
      If one piece of computational work prevented others from utilizing the system,
        then there would be little mutual benefit provided to these volunteers.
      For example,
        if one user launches a computation that takes days to complete,
        other users should not be left unable to perform their own work.
    }
  \end{enumerate}


\section{Overview and Structure}
  The structure of this report does not stray to far from those commonly used in Software Engineering practices.
  Below you shall find a brief description of each chapter,
    outlining the content contained within each.
  These descriptions exclude the contents listings,
    introduction,
    conclusion,
    and all subsequent content matter.

  \subsection*{Literature Review}
    Chapter 2 will present you with relevant literature research.
    There shall be definitions of all the major technological areas focused on within this investigation,
      and a history of how they came to be.
    After,
      there shall an evaluation of their rationale,
      associated problems,
      comparisons of relevant work,
      as well as leanings obtained from their analysis.

  \subsection*{Requirements}
    Chapter 3 contains elicitation and analysis of the project's requirements.
    This includes contents such as the use cases,
      acceptance criteria,
      and functional and non-functional requirements of the system.
    There will also be reasoning justifying the chosen paradigm for managing the development of the project.

  \subsection*{Design}
    The requirements are followed by Chapter 4,
      which includes the high and low level designs of the software system.
    This chapter shall turn the requirements into a more concrete plan,
      preparing for the implementation of the system.

  \subsection*{Implementation}
    In Chapter 5,
      presented to you will be the actual implementation of the system.
    This chapter will demonstrate the resulting application.
    As a major consideration, 
      it will also explain the rationale for any changes made to the original design.

  \subsection*{Evaluation}
    Finally, 
      in Chapter 6,
      there will be an evaluation of the software,
      comparing the system's requirements and design to its actual implementation. 
    This will include analysis of any problems or issues encountered,
      descriptions of the limitations of the system,
      and an exploration of future work that could be accomplished.

