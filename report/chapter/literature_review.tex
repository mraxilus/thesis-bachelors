\chapter{Literature Review}

\section{Definitions}
  \begin{description}
    \item[Volunteer Computing,]{
        is the idea of pooling a multitude of potentially idle workstations for the purposes of performing parallel processing.
        An activity considered as an attractive alternative to purchasing,
        or implementing a large supercomputer~\autocite{article:vc}.

      It has also been defined as a form of Distributed Computing with which volunteers,
      being ordinary people spread throughout the Internet,
      can donate their computing resources to research projects~\autocite{article:vccmri}.

      Beberg et al. expand upon these definitions by stating that Volunteer Computing is preformed by Internet computers, ``not on machines under the control and management of the project''~\autocite{article:flfeyvdc}.
      Unlike the previous definitions,
        this places explicit restrictions on which computers can be volunteered.

      Henceforth,
        Volunteer Computing shall be thought of as the concept of performing parallel processing by pooling a myriad of potentially idle workstations.
      Workstations which are volunteered by ordinary people across the Internet.
      This definition does not enforce the restriction placed by Beberg et al.,
        but rather,
        requires that the amount work performed by the volunteered workstations must outweigh machines under the direct control of the project or individuals who receive computational benefit.

    }
    \item[Peer-to-Peer,]{
        by one definition,
        involves nodes of a network who all share a part of their hardware resources,
        which are needed to fulfill the service offered by the network as a whole.
      Within this network,
        each node must be accessible by its peers directly.
      The end result is that each peer is both the requester,
        and provider of resources~\autocite{article:adppncppaa}.

      Singh states that,
        ``P2P can be defined most easily in terms of what it is not: the client-server model''.
      He goes on to define Peer-to-Peer Computing as an application which is split into multiple equal parts.
      Whilst not exhaustive with his definition,
        he analyses the potential benefits of Peer-to-Peer systems.
      For example,
        because of their decentralized nature they would be harder to shutdown,
        thus potentially withstanding repressive regimes~\autocite{article:pppc}.

      A Peer-to-Peer network can also be said to exist when multiple connected computers share resources.
      This must be done via direct means,
        rather than going through a separate server.
      Networks of this type can be connected in an ad hoc,
        or permanent manner.
        Nodes can even communicate directly over a large medium such as the Internet~\autocite{website:qsppn}.

      Using this knowledge,
        Peer-to-Peer,
        as it relates to this investigation,
        shall be known as a network of of nodes who all share their hardware resources.
      Each node must be able to directly communicate with every other node in the system.
      Nodes should be able to join and leave the network in an ad hoc fashion without disruption to the workings of other nodes.
    }
    \item[MapReduce]{
        in its original paper was described as,
        ``a programming model and an associated implementation for processing and generating large data sets.''
      It accomplishes this via means of,
        as the name suggests,
        a map and a reduce function.
      The map function processes key/value pairs to retrieve intermediary values,
        and the reduce function merges the intermediary values with matching keys together to produce the final result~\autocite{article:mrsdplc}.

      Lin et al. define MapReduce as offering,
        ``a flexible programming model for processing and generating large data sets on dedicated resources.''
      They refine their definition by noting that it is used where only a small fraction of the resources being processed are unavailable at any given time~\autocite{article:moonmroon}.

      MapReduce has also been defined as an emerging programming model,
        borrowing ideas from functional programming,
        namely the map and reduce higher-order functions~\autocite{article:tmrdgc}.

      MapReduce,
        from this point forth,
        shall thought of as a programming model for the processing and generating of large data sets.
        A model which accomplishes these tasks via an adaptation of the map and reduce higher-order functions (see Appendix~\ref{appendix:hof}).
    }
  \end{description}

\section{History}
  \subsection{Volunteer Computing}
    Existing for a little over a decade,
      the concept of Volunteer Computing is relatively new,
      when compared with its forefather,
      Distributed Computing. 
    Some Distributed Computing projects,
      such as the Condor scheduling system,
      began to hit on the concept of pooling unused computing capacity in as far back as the 1980's~\autocite{article:cahiw}.
    The idea of using the computers of ordinary individuals did not occur until much later.
    It must be noted of course,
      that at a time preceding the bloom of the internet,
      implementing such a concept such as Volunteer Computing would have seemed infeasible.

    The term Volunteer Computing,
      as it is known today,
      was originally coined by Luis F. G. Sarmenta in 2001~\autocite{article:vc},
      however projects fitting the description have existed since the 1990's,
      namely the Great Internet Mersenne Prime Search.
    GIMPS,
      as the name suggests,
      was developed in 1996 to aid in the search for Mersenne primes utilising the power of the Internet~\autocite{website:gimps}.

    Sartmenta compared this particular type of computing to a common tradition in the Philippines.
    This tradition,
      called Bayanihan,
      involves members of a community banding together to helping each other move houses (see Figure~\ref{figure:bayanihan}).
    That is to say,
      using bamboo stalks and an enormous amount of man power,
      they literally move their houses~\autocite{article:vc}.
    Demonstrating that enormous accomplishments can be made from the simple act of resource pooling.
    In its essence, that is the motive behind Volunteer Computing systems. 

    \begin{figure}[h]
      \centering
      \includegraphics[width=0.8\textwidth]{image/bayanihan.jpg}
      \caption[The Filipino Bayanihan tradition]{
        The Filipino Bayanihan tradition involves the relocation of houses using bamboo poles,
        and dozens of men~\autocite{website:fbjpg}.
      }
      \label{figure:bayanihan}
    \end{figure}

    More recently,
      in 2004,
      the developers of the SETI@home created the Berkeley Open Infrastructure for Network Computing to empower other scientists to be able to take advantage of the Volunteer Computing paradigm.
    BOINC supports a wide variety of applications,
      allowing volunteers to participate in multiple projects,
      and enabling them to specify the amount of resources they would prefer to allocate to each project~\autocite{article:boincasprcs}.
      The BOINC project has allowed the @home movement to flourish. 
      Research projects using the system include Asteroids@home,
        LHC@home,
        Milkyway@home,
        Rosetta@home,
        and many more~\autocite{website:cboincp}.
      Currently, Volunteer Computing has become all the more enticing as researchers no longer have to implement their own systems from the ground up.
        

  \subsection{Peer-to-Peer}
    The fundamental principles of Peer-to-Peer computing have been around for quite some time.
    When originally conceived in the 1960's,
      the Internet as it was, 
      was in fact,
      a Peer-to-Peer system.
    The ARPANET was created with the intention of sharing computer resources via the integration of existing networks.
    The goal was to create the ARPANET in such a way that allows every host to be an equal peer~\autocite{book:pphpdt}.

    As the Internet grew Peer-to-Peer systems such as Usenet and the Domain Name System became common place.
    At this time the Internet was a fairly safe,
      cooperative place.
    However,
      on the date of April, 
      1994,
      Usenet was hit with the notorious ``Green Card Spam'' attack,
      forever destroying the innocence of the medium known as the Internet~\autocite{website:tstsia}.
    Try as they might,
      to this day there has never been an effective means developed to prevent spam.
    One major lesson learnt that year is that,
      given Peer-to-Peer systems,
      rules of social responsibility are extremely difficult to enforce~\autocite{book:pphpdt}.

    The notoriety of Peer-to-Peer computing would then arise again in the 1999,
      with the creation of the Napster file sharing service.
    This hugely controversial service enabled its users to easily share MP3 file across the Internet.
    Ultimately succumbing to legal pressure Napster collapsed,
      leaving in its wake a host of clone applications such as Freenet,
      and Gnutella.
    Although Peer-to-Peer systems are notorious for supporting illegal activities such as pirating,
      the technology itself has been cited as being long-lasting and uncontroversial~\autocite{article:ppn}.

    The ideas behind Peer-to-Peer computing have sparked the creation of the Internet,
      and new fields such as Distributed and Volunteer Computing.
    Researchers see Peer-to-Peer as an important technology having a future role in the creation of new algorithms,
      applications,
      and platforms~\autocite{article:ppc}. 
  
  \subsection{MapReduce}
    In the early 2000's Google,
      being a company that focuses on big data,
      had implemented many specialized computations to process large amounts of it gathered on the structure of the Internet.
    The computations themselves were rather trivial,
      but they soon realized that the complexities involved in parallelizing and distributing these computations warranted the development of a solution.
    Due to this need,
      in 2004, 
      Dean and Ghemawat went on to develop an abstraction,
      allowing them to express these computations without having to concern themselves with the details of parallelization.
    They called this abstraction MapReduce,
      as it was based off of the concepts of the higher-order map and reduce functions,
      borrowed from functional programming languages~\autocite{article:mrsdplc}.

    A few years later,
      a distributed computing platform was developed in 2006,
      incorporating the MapReduce paradigm.
    This platform was called Hadoop.
    Hadoop is now an Apache project which was heavily contributed to by Yahoo!.
    Yahoo! reports that over one hundred other organizations are utilising Hadoop for their own purposes.
    They themselves having a Hadoop cluster of 25,000 servers which deals with 25 petabytes of data~\autocite{article:thdfs}.

    Google themselves now offer a Hadoop service on their Google Cloud Platform,
      citing that it offers quick startup times,
      fast I/O operations,
      and scalability~\autocite{website:hsfgcp}.
    In 2011,
      they also released experimental python support for their Google App Engine~\autocite{website:mrpo}.
    So far MapReduce has not reached the apex of its usefulness.

    The use of MapReduce is becoming increasingly more popular for large scale parallel computations.
    Google attributes its success to its ease of use,
      the fact that a large range of problems are easily expressed,
      and because of its extreme scalability on clustered systems~\autocite{article:mrsdplc}.
      

\section{Relevancy}
  Now take into consideration how these concepts are relevant to this particular project.
  Taken individually you may be able to see the relevance,
    however its still unclear as to how they could work together in a single project.
  At first glance these ideas may seem entirely unrelated,
    but rest assured,
    they come together in quite synergistic ways.
    
  \subsection{Related Work}
    Recently, 
      a paper entitled, 
      ``Towards MapReduce for Desktop Grid Computing,'' was written concerning ideas touched on throughout this investigation.
    Desktop Grid Computing is a term that shares some similarities with Volunteer Computing.
    The differences being that,
      in Desktop Grids,
      computing resources can be trusted,
      the computation can be completely out of the control of the workstation user,
      and the deployment of the system is usually automated~\autocite{website:gcboinc}.
    Ultimately,
      the only significant difference between Volunteer Computing and Grid Computing is whether or not the workstations used can be trusted.


    As examples,
      SETI@home would not be considered a Grid Computing project since the workstations of anonymous volunteers over the Internet are inherently untrustable.
    The previously discussed Condor scheduling system,
      however,
      would fit within that category for the following reasons~\autocite{article:cahiw}:
    % TODO(mraxilus): could take this list out and just state that its because they have control of pcs.
    \begin{itemize}
      \item{
        Only workstations from within the University department of the system's developers were used,
          which implies that they were trusted.
      }
      \item{
        For the most part,
          users of their workstations remained unaware of the background computations being performed.
      }
      \item{
        The system was not installed by the workstations' users,
          but rather it was installed automatically by the system developers.
      }
    \end{itemize}

    In 2010,
      Tang et al. went on to analyse the possibility of utilising MapReduce on Grid Computing systems.
    This was accomplished by means of the development of a prototype system.
    They showed that it is possible to create an efficient and secure MapReduce runtime for use on Desktop Grids.
    Tang et al. states that their prototype features massive fault tolerance,
      barrier-less MapReduce,
      and distributed results checking.
    Foreshadowing its potential in Volunteer Computing settings,
      they believe that this approach proves to be suitable for Desktop Grids over the Internet~\autocite{article:tmrdgc}.

    The following year,
      researchers continued their work by considering the potential for MapReduce under the Volunteer Computing paradigm.
    Their key focus was on distributed results checking,
      that is,
      ensuring that results remain valid even amongst untrustable volunteers.
    This research resulted in the creation of a model for the error rate of MapReduce used in this context.
    They plan to continue efforts on this front as they still believe it to be a promising approach~\autocite{article:drcmrvc}.

  \subsection{Problems}
    The area of computing focused on in this investigation is relatively new.
    To this day its viability is still being explored and worked upon.
    Existing solutions such as Hadoop have been demonstrated to be unsuitable for Desktop Grid Computing,
      let alone Volunteer Computing~\autocite{article:tmrdgc}.

    No solution is without its downsides. 
    Desktop Grid Computing requires large amounts of infrastructure and system maintenance.
    On the other hand,
      Volunteer Computing raises security and reliability issues due to untrusted,
      and often volatile,
      peers.
    

\section{Learnings}
  \subsection{Search Strategies}
    Initially,
      the research strategy in this investigation could have been compared to playing Darts.
    That is to say,
      the initial strategy involved searching aimlessly for papers with relevant keywords using Google Scholar,
      all in the hopes of discovering suitable papers.
    After wasting large amounts of time with little progress,
      a more structured approach was needed.

    Unfortunately,
      due to a lack of knowledge,
      a similar approach was utilised to discover more effective search strategies.
    In 2012,
      Code Capsule published an article which demonstrates an effective method of implementing algorithms from scientific papers~\autocite{website:hiafsp}. 
    Goossaerts article,
      whilst extremely useful,
      references a paper more appropriate to this problem.
      The referenced paper outlines and describes an effective search strategy for researchers.

    ``How to Read a Paper'' discusses an efficient three-pass approach for performing literature reviews.
    The author of this paper notes that search strategies are skills that are rarely taught,
      leading to tremendous amounts of wasted effort~\autocite{article:hrp}.
    After adopting the strategy proposed in the paper,
      this investigation's research be came a much more effective endeavour.

    Ultimately,
      the major resources utilised were a collection of databases and search engines such as Google Scholar,
      IEEE Xplore, Springer, and the ACM Digital Library.

  \subsection{Notable Authors}
    During the research segment of this study it became evident as to which authors were most notable in each field of interest.
    The following are descriptions of these authors,
      stating to which fields they contribute to,
      and listing some of their works relevant to this study:
    \begin{description}
      \item[Luis F. G. Sarmenta]{
          is a pioneer in the Volunteer Computing field.
        He fully developed the term in his Ph.D. thesis entitled ``Volunteer Computing''~\autocite{website:plfgspd}.
        Sarmenta has also published many related works such as:
        \begin{itemize}
            \item Web-Based Volunteer Computing Using Java
            \item Towards Bayanihan: Building an Extensible Framework for Volunteer Computing Using Java
            \item Sabotage-Tolerance Mechanisms for Volunteer Computing Systems
        \end{itemize}
      }
      \item[Andy Oram]{
          is an extremely influential writer in the field of Peer-to-Peer computing.
        Since 1992,
          Oram has been an editor at O`Reilly Media.
        Published in 2001,
          his book,
          entitled ``Peer-to-Peer'' helped to explore and define the field as it is known today~\autocite{website:ao}.

        Another one of his famous works includes Beautiful Code,
          a book amalgamating the ideas of various authors into a single source.
        Beautiful code happens to contain a chapter called,
          ``Distributed Programming with MapReduce'' by Jeffery Dean and Sanjay Ghemawat.
        This particular chapter explores the MapReduce model,
          and its uses~\autocite{book:bc}.
      }
      \item[Jeffery Dean and Sanjay Ghemawat]{
          have been working at Google since 1999,
          and were both heavily involved in design and implementation of MapReduce~\autocite{website:jd}.
        They have published many papers on the subject including:
        \begin{itemize}
          \item MapReduce: Simplified Data Processing on Large Clusters
          \item Distributed Programming with MapReduce
          \item MapReduce: a flexible data processing tool
        \end{itemize}
      }
      \item[Mircea Moca and Gilles Fedak]{
          are two authors who's current work is likely to be the most relevant to this research.
        They are continually exploring the potential for utilising the MapReduce model in Desktop Grid and Volunteer Computing settings.
        Their work includes:
        \begin{itemize}
          \item Towards MapReduce for Desktop Grid Computing
          \item Distributed Results Checking for MapReduce in Volunteer Computing
        \end{itemize}
      }
    \end{description}

  \subsection{Outcomes}
    Researchers have demonstrated that there exists opportunities to utilise the spare capacity of Internet workstations for the purposes of Volunteer Computing.
    From high end gaming rigs to low-cost media centres,
      these internet devices are everywhere in today's society.
    The map shown in Figure \ref{fig:wmipaai} demonstrates the sheer magnitude of the maximum capability that Volunteer Computing could potentially achieve.
    As the demand for more intensive parallel applications rises,
      the concept of utilising existing resources as a means of gaining more computational power becomes more enticing.

    \begin{figure}[h]
      \centering
      \includegraphics[width=0.8\textwidth]{image/world_map_high_resolution@1600x900.png}
      \caption[World map of IP addresses across the Internet.]{
        World map of IP addresses across the Internet which responded to ICMP requests or port scans.
        To give a greater sense of scale,
          this map goes from blue to red,
          blue signifying the low end of 0 to 1,000 IP addresses,
          and red being anything within the range of 1 to 10 million,
          perhaps greater~\autocite{website:ic2012}.
      }
      \label{fig:wmipaai}
    \end{figure}

    Implementing a low cost cluster,
      such as the 32 node cluster at Boise State University,
      may not be financial affordable for many individuals who would benefit from such a system.
    The other alternative,
      configuring pre-existing Volunteer Computing software platforms such as BOINC may prove inappropriate for a few reasons:
    \begin{itemize}
      \item {
        Creation of projects require significant configuration.
        Project developers are required to configure databases,
          modify XML project files,
          compile BOINC from source,
          create and manage Work Units,
          and start multiple server daemons~\autocite{article:bdaboinc}.
        Many of the aforementioned deal with domain specific knowledge which would require some significant time investment.
      }
    \item {
      Once established,
        in order for a project to acquire resources they must explicitly acquire volunteers.
      Using the BOINC system,
        individuals must manually volunteer their idle time to your project by means of either an account manager external to the system,
        by word-of-mouth,
        or by using a search engine~\autocite{website:am}.
      Volunteers have to expel quite a bit of effort to support any new projects,
        which means these project have to raise awareness in order to acquire enough donors to benefit from the Volunteer Computing paradigm.
    }
    \end{itemize}

    This study shall solve all of these issues as Developers will be able to connect to an established network of Volunteers with minimal system configuration using only a single low-cost device.
    These goals can be accomplished by using the MapReduce paradigm to provide an abstraction for work assignment and delegation,
      allowing developers to solely focus on the development of their map and reduce functions,
      as opposed to modifying boilerplate configurations to suite their specific use case.
