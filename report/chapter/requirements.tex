\chapter{Requirements}
\section{System Domain}
As previously discussed,
  Volunteer Computing systems have been able to solve the problems when faced with processing large amounts of data which can be parallelized.
The concept of these sort of systems have been taken advantage of by various large scale scientific projects,
  such as SETI@home,
  however it is an area of computer science that still remains untapped by students,
  startups,
  individuals,
  and small research projects.

Despite advances with configurable distributed software,
  it still remains too complicated for those looking to quickly prototype a concept. 
Not only that,
  but volunteers have multiple barriers when it comes to supporting other users projects.
Folding@home has attempted to alleviate this problem,
  they do this by providing a setup process for Volunteers which involves nothing more than downloading and installing their client~\autocite{website:fhlig}.

Folding@home has made their process much simpler than BOINC,
  however the problem still exists in this domain.
Being specific to protein folding,
  the Folding@home project does not offer general purpose Peer-to-Peer volunteering capabilities.
There is an opportunity in the area of Volunteer Computing that needs to be fulfilled.

A system is defined as ``an assemblage or combination of things or parts forming a complex or unitary whole~\autocite{website:s}.''
In this particular context,
  the assemblage of things would refer to the devices running the software,
  the stakeholders or those affected by the software,
  and the data and functions with the software delegates and processes.
The system,
  so forth,
  shall be known as the combination of devices,
  stakeholders,
  data,
  and functions which together comprise the Peer-to-Peer Volunteer Computing arrangement which produces benefit for the target audience of this investigation,
  as specified in the introduction.


\section{Stakeholders}
  The stakeholders of this system are any individuals who are either directly or indirectly affected by the system in some meaningful way.
  Three major stakeholders to be considered are developers who use the system,
    volunteers who donate idle capacity to the system,
    and bystanders who are users of devices running the system.
  Any individual can at different times act not only as one,
    but as multiple of these stakeholders.
  Take for example a developer who,
    whilst not submitting work of their own,
    becomes a volunteer by donating their resources to other developers.

  For the sake of simplicity,
    the system stakeholders shall be modeled each role at a time.
  Whilst this strategy may not accurately reflect the complexity of the variation in stakes for a single individual,
    it allows for the classification of clear and concise general cases which,
    taken in their entirety,
    can encompass the requirements of all of the system's stakeholders.
  Taking the aforementioned stakeholders groups in more detail,
    there exists:


  \begin{description}
    \item[Developers] {
        who create work for the system to delegate and process.
      They accomplish this via means of the creation of map and reduce functions,
        with which the system is designed to operate.
      Developers can be considered the largest benefactor of the system,
        as they are the ones who gain increased processing power for their projects,
        with no added costs incurred on their part.
      In order for the developers to achieve a net positive gain in computational ability,
        the ratio of Developers to Volunteers must be low enough to offset the communication and distribution overhead created by the system itself.
    }
    \item[Volunteers] {
        are individuals across the internet who donate their spare idle computational capacity for developers to utilise.
      More specifically,
        volunteers are persons who install the system on their personal devices,
        and run it without submitting any work of their own to be delegated to other devices on the system's network.
      Effectively,
        volunteers are the users of the system who ensure that the system provides benefit by ensuring that their exists spare capacity to be distributed.
      Without enough volunteers,
        the system would be no more effective,
        and possibly less effective,
        than if each developer were to drop their usage of the system all together.
    }
    \item[Bystanders] {
        are people who are co-users of a device that runs the system.
      They may not have any direct involvement with the system itself,
        however bystanders use devices which are part of the system for external purposes.
      Take for example a family which has a low-cost device setup as media centre,
        if this device were to also be a part of the system,
        bystanders would be individuals who watch media on the device,
        yet remain unaware of the system's presence.
    }
  \end{description}

\section{User Stories}
  This study is to use the Agile methodology as its basis for requirements elicitation and analysis.
  Following the practices of the Agile process,
    requirements elicitation shall be carried out via the creation and elaboration of what is known as user stories.
  Every user story is designed such that a single stakeholder goal and its reasoning is captured.

  The following story cards are going to use the common template,
  ``As a [user], I want [goal], so that [reasoning].''
  Where the user is the current stakeholder being considered,
    the goal is their objective for this particular requirement,
    and the reasoning is their rationale for why the goal needs to be met.
  All of the requirements of the system shall be captured in this way,
    and then discussed and elaborated upon.

  There are a few reasons why this method has proven to be the most popular in the Software Engineering field.
  By putting each individual stakeholder in first person it allows team members to closely relate to their situation,
    which also encompasses a greater understanding of their goals and desires.
  Additionally,
    having a consistent structure for every created user story will aid in the prioritization and planning of the system's requirements,
    as without a consistent structure it becomes harder to understand what a feature is,
    who the feature benefits,
    and what value the feature provides to the system as a whole~\autocite{website:aauwust}.


  \begin{figure}[h]
    \centering
    \includegraphics[width=0.8\textwidth]{image/user_story_card_example@1024x698.png}
    \label{figure:amo}
    \caption[An example of a physical story card.]{
      This is an example of a story card for a help desk system,
        which illustrates the goal to allow operators to search for customers by name,
        with the reasoning that it shortens response times~\autocite{website:arsfuss}.
    }
  \end{figure}


  \subsection{As Developers}
    From the perspective of a developer in the system,
      one of the more obvious requirements has been captured in Table \ref{table:scdis}.
    In order for the system to be usable the developer would require some way of importing the system as either a library or package.
    With that being said,
      there are other methods of configuring a system such as this,
      for example the BOINC platform provides a client which users download,
      compile,
      and configure separate from any library or package management system.


    \begin{table}[h]
      \centering
      \caption{Story Card --- Developer imports the system.}
      \label{table:scdis}
      \begin{tabular}{p{0.8\textwidth}}
        \toprule
        As a Developer,
          I want to import the system as a library,
          so that it is easier to configure and utilise when compared to BOINC. \\
        \bottomrule
      \end{tabular}
    \end{table}

    The requirement that this card captures is that the system in question should provide a simplistic means of configuration and usage.
    Rather than forcing developers to modify external configuration files,
      configuration should be minimized an achievable from within the developer's source code.
    The goal of this user story can be achieved by ensuring that developers can install and run the system without having to learn and understand the different possible configurations available.

    \begin{table}[h]
      \centering
      \caption{Story Card --- Developer connects to existing network.}
      \label{table:scdcen}
      \begin{tabular}{p{0.8\textwidth}}
        \toprule
        As a Developer,
          I want to be able to connect to an existing network of devices using the system,
          so that I can gain access to greater parallel computational power than my device can provide. \\
        \bottomrule
      \end{tabular}
    \end{table}

    After the developer has imported the system they must connect to an existing network to acquire more computational power,
    the story card in Table \ref{table:scdcen} captures this need.
    Without having the ability to connect to pre-existing networks of both developers and volunteers,
      the value of the system would be diminished as one would need to own multiple devices to obtain the benefits of distributed computing.
    In the situation where the system would allow developers to connect to existing networks,
      they would instantly acquire access to assemblage of volunteers,
      and thus making this system preferable to BOINC,
      that is,
      in terms of time and effort put into the acquisition of volunteers.

    \begin{table}[h]
      \centering
      \caption{Story Card --- Developer distributes work using MapReduce.}
      \label{table:scddwumr}
      \begin{tabular}{p{0.8\textwidth}}
        \toprule
        As a Developer,
          I want to be able to distribute work across the network by defining Map and Reduce functions,
          so that my code is tied to the MapReduce abstraction rather than a domain specific paradigm. \\
        \bottomrule
      \end{tabular}
    \end{table}

    Recent distributed computing systems,
      such as Apache Hadoop,
      have relied on an abstracted programming model for work assignment,
      rather than a domain specific interface.
    In the case of Apache Hadoop,
      MapReduce simplifies data processing for the developer,
      however this shift has not occurred in the context of Volunteer Computing.

    Refering to Table \ref{table:scddwumr},
      developers of the system should be able to create map and reduce functions in a domain agnostic manner.
    Developers concepts and implementation should remain generalized enough so that transferring their computations to other MapReduce compatible distributed platforms requires almost no modifications to their core algorithms.
    \FloatBarrier

  \subsection{As Volunteers}
    One of the reasons why volunteers would exist in this system is because many people would like to avoid wastage,
      and by not utilizing their devices to their fullest extent,
      potential computational power is discarded.
    Individuals who recognise the need for technological improvement should be delighted to donate processing time they would otherwise not have used.

    \begin{table}[h]
      \centering
      \caption{Story Card --- Volunteer donates spare processing capacity.}
      \label{table:vdspc}
      \begin{tabular}{p{0.8\textwidth}}
        \toprule
        As a Volunteer,
          I want to donate spare processing capacity of my device to other users of the system,
          so that they can gain more parallel computational power than they had previously. \\
        \bottomrule
      \end{tabular}
    \end{table}

    Proof of this phenomenon can be seen in vast amalgamations of volunteers supporting @home movements,
      even taking into consideration the effort involved becoming a volunteer for one of those projects.
    Clearly,
      there exists individuals who would become a part of the system by donating their devices processing time.
    This requirement is captured under the story card in Table \ref{table:vdspc},
      which asserts that volunteers would willingly donate their spare processing capacity to the system.

    Many Volunteer computing projects offer some form of recognition to their volunteers.
    This can come in the form of a credit or point based system.
    The creators of Folding@home believe that having a point based system is a key aspect of many distributed computing projects.
    This is because points indicate to donors their level of contribution,
      and they facilitate friendly competition amongst donors~\autocite{website:faqp}.

    \begin{table}[h]
      \centering
      \caption{Story Card --- Volunteer tracks delegated work.}
      \label{table:scvtdw}
      \begin{tabular}{p{0.8\textwidth}}
        \toprule
        As a Volunteer,
          I want to track what work my device gets assigned and from where,
          so that I can monitor my overall contributions to the system. \\
        \bottomrule
      \end{tabular}
    \end{table}

    Whist it may not be necessary to implement a credit or point based system,
      volunteers should be able to ascertain how much work is being assigned to and run on their devices.
    Table \ref{table:scvtdw} outlines a story card which encompasses this idea.
    Volunteers should be able to track how much they are contributing to the system so that they can verify that they are making a difference.
    Without this information they would not be able to determine whether or not the system is even using their device for work.

    The following story card in Table \ref{table:scvirs} is similar to those in both Table \ref{table:scdis} and Table \ref{table:scdcen},
      however it is taken from the perspective of a volunteer as opposed to a developer.
    It is imperative that volunteers can install and run the system just as easy,
      if not easier than developers,
      as the goal is to acquire as great as a volunteers per developer ratio as possible in an effort to maximise the effectiveness of the system.
    Volunteers may not necessarily comprehend the details of MapReduce and Volunteer computing,
      and thus the volunteer process should ideally be downloading, installing, and running the system's software without any additional configuration.

    \begin{table}[h]
      \centering
      \caption{Story Card --- Volunteer installs and runs the system.}
      \label{table:scvirs}
      \begin{tabular}{p{0.8\textwidth}}
        \toprule
        As a Volunteer,
          I want to be able to install and run the system with minimal effort,
          so that I can contribute resources to multitudes of projects without having to explicitly join them individually. \\
        \bottomrule
      \end{tabular}
    \end{table}

    Volunteers may also be unaware as to the network topology of the system so,
      just as it was the case with developers,
      volunteers must be able to join existing networks automatically upon instantiation of an instance of the software.
    This insures that even volunteers with practically no domain specific knowledge,
      in the area of distributed processing,
      can donate their resources without having to hurdle any significant barriers to entry.
    If volunteers do happen to have experience in the area of volunteer computing,
      the ad hoc nature of the system should allow the system to remain flexible.
    \FloatBarrier

  \subsection{As Bystanders}
    A bystander utilises a device running the system's software for other purposes.
    They must be given the assurance that the system will not prove to be greedy with their device's resources,
      and disrupt the functionally or reliability of other applications.

    \begin{table}[h]
      \centering
      \caption{Story Card --- Bystander requires usage of other applications.}
      \begin{tabular}{p{0.8\textwidth}}
        \toprule
        As a Bystander,
          I want the system to only utilise idle computational capacity for delegated work,
          so that I can run other applications on my device without disruption. \\
        \bottomrule
      \end{tabular}
    \end{table}

\section{Prioritization}
  One of the major focus of Agile practices is to deliver frequently~\autocite{website:agilemethodologyadfkickstart},
    and a context with which this methodology proves to be effective is when a concept is charting new territory,
    thus the requirements require continual refinement and prioritization as more knowledge of the system is gained.
  Now that the user stories have been established they must be given relative priority as to allow the production of the core components of the system before implementing any additional features.

  From the developers perspective,
    importing the system in a simplistic manner is not as important as either distributing work in an abstracted model such as MapReduce,
    or seamlessly connecting to an existing network of volunteers.

  \begin{table}[h]
    \centering
    \caption{User story priority assignment.}
    \label{table:pus}
    \begin{tabular}{ll}
      \toprule
      User Story & Priority \\
      \midrule
      Developer imports the system. & Medium \\
      Developer connects to existing network. & High \\
      Developer distributes work using MapReduce. & High \\
      Volunteer donates spare processing capacity. & High \\
      Volunteer tracks delegated work. & Low \\
      Volunteer installs and runs the system. & Medium \\
      Bystander requires usage of other applications. & Low \\
      \bottomrule
    \end{tabular}
  \end{table}

  This is the case since the functionality of the core components of the system is considered to be a prerequisite to the improvement of usage complexity. 
  Another feature central to the goals of this system is to make it possible for volunteers donate the processing power of their devices,
    thus it is rated highly.
  Although not as important,
    having volunteers be able to install and run the system without any significant barriers is vital for amalgamating more donors.

  \begin{table}[h]
    \centering
    \caption{Prioritized user stories.}
    \label{table:pus}
    \begin{tabular}{ll}
      \toprule
      Order & User Story \\
      \midrule
      0 & Developer distributes work using MapReduce. \\
      1 & Volunteer donates spare processing capacity. \\
      2 & Developer connects to existing network. \\
      3 & Developer imports the system. \\
      4 & Volunteer installs and runs the system. \\
      5 & Volunteer tracks delegated work. \\
      6 & Bystander requires usage of other applications. \\
      \bottomrule
    \end{tabular}
  \end{table}

  Tracking work that gets delegated to each volunteers device may provide incentive for volunteers to contribute and recruit others,
    however it is not central to the core functionality of the system.
  That along with ensuring that bystanders do not experience significant disruption of their other applications,
    is of relatively low priority to the aforementioned requirements.
