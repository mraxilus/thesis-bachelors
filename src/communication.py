#!/usr/bin/env python

import argparse as a
import collections as col
import communication_pb2 as c
import itertools as i
import functions as f
import random as r
try:
  import RPi.GPIO as g
except ImportError:
  pass
import socket as s
from SocketServer import BaseRequestHandler, TCPServer
import threading as th
import time as ti
import uuid as u

index = set()
intermediates = []
results = []
task_scope = dict([(function, eval(function)) for function in 
  ['len', 'range', 'sum']
])
workers = 0


class MasterHandler(BaseRequestHandler):
  def handle(self):
    global index
    global intermediates
    global results
    try:
      packet = f.recieve_packet(self.request)
      index.update(list(packet.peers))
      f.print_packet(packet, 'master: result ')

      # update results.
      result = eval(str(packet.task.result))
      if packet.task.type == c.Task.MAP:
        intermediates.append(result)
      elif packet.task.type == c.Task.REDUCE:
        results.append(result)
    except Exception as exception:
      print('error: ' + str(exception))


class WorkerHandler(BaseRequestHandler):
  def handle(self):
    global index
    global task_scope
    global workers
    try:
      # update worker count.
      workers += 1
      f.change_status('red')

      packet = f.recieve_packet(self.request)
      index.update(list(packet.peers))
      f.print_packet(packet, 'worker: receive ')

      # setup peers from index.
      packet.ClearField('peers')
      packet.peers.extend(list(index))

      if packet.task.type != c.Task.NONE:
        # evaluate the result.
        task_scope['key'] = eval(str(packet.task.key))
        task_scope['value'] = eval(str(packet.task.value))
        exec str(packet.task.function) in {'__builtins__': None}, task_scope
        packet.task.result = str(task_scope['result'])

        # send back packet with updated result.
        source = packet.source.split(':')
        f.send_packet(packet, host=source[0], port=source[1])
      else:
        f.send_packet(packet, self.request)
    except Exception as exception:
      print('error: ' + str(exception))
    finally:
      # reset status if no work left.
      workers -= 1
      if workers == 0:
        f.change_status('green')


def sumbit(data_map, mapfn, reducefn, host, port):
  global index
  global intermediates
  global results

  # prepare for worker selection.
  r.seed() 
  index.add(host + ':' + port)

  # setup retrieval server.
  host_origin = str([(sock.connect(('8.8.8.8', 80)), sock.getsockname()[0], sock.close()) for sock in [s.socket(s.AF_INET, s.SOCK_DGRAM)]][0][1])
  source =  host_origin + ':8888'
  server = TCPServer((host_origin, 8888), MasterHandler)
  thread = th.Thread(target=server.serve_forever, args=[])
  thread.start()
  
  # gather peers.
  job = str(u.uuid4())
  socket = s.socket(s.AF_INET, s.SOCK_STREAM)
  try:
    socket.connect((host, int(port)))
    packet = f.create_packet(source)
    f.send_packet(packet, socket)
    packet = f.recieve_packet(socket)
    index.update(list(packet.peers))
  finally:
    socket.close()

  # perform map delegation.
  distribute_tasks(source, job, data_map, mapfn, c.Task.MAP)
  while len(intermediates) < len(data_map):
    ti.sleep(0.01)

  # process the intermediate results.
  data_reduce = col.defaultdict(list)
  for key, value in list(i.chain.from_iterable(intermediates)):
    data_reduce[key].append(value)

  # perform reduce delegation.
  distribute_tasks(source, job, data_reduce, reducefn, c.Task.REDUCE)
  while len(results) < len(data_reduce):
    ti.sleep(0.01)

  server.shutdown()
  thread.join()
  return results


def distribute_tasks(source, job, data, function, task_type):
  global index
  pool=[]
  for key, value in data.items():
    # ensure clients are assigned even amount of work.
    if len(pool) == 0:
      pool=list(index)
      r.shuffle(pool)

    # distribute a task to a single remaining client.
    destination = pool.pop().split(':')
    task = f.create_task(job=job,
                         key=key,
                         value=value,
                         function=f.convert_function_to_string(function),
                         task_type=task_type)
    packet = f.create_packet(source, task, list(index))
    f.print_packet(packet, 'master: assign ')
    f.send_packet(packet, host=destination[0], port=destination[1])


if __name__ == '__main__':
  # parse command line arguments.
  parser = a.ArgumentParser()
  parser.add_argument('host', type=str, default='localhost',
                      help='address to host the worker on.')
  parser.add_argument('port', type=int, default=8889, 
                      help='port to use for incoming connections.')
  parser.add_argument('--peer_host', type=str, 
                      help='address of peer in order to grow index.')
  parser.add_argument('--peer_port', type=int, 
                      help='port of peer in order to grow index.')
  args = parser.parse_args()

  # setup status indicators.
  g.setmode(g.BOARD)
  g.setwarnings(False)
  g.setup(12, g.OUT)
  g.setup(16, g.OUT)
  g.setup(18, g.OUT)
  f.change_status('green')

  # initialize index from peer worker.
  if args.peer_host is not None and args.peer_port is not None:
    socket = s.socket(s.AF_INET, s.SOCK_STREAM)
    try:
      socket.connect((args.peer_host, int(args.peer_port)))
      source = args.host + ':' + str(args.port)
      packet = f.create_packet(source)
      packet.peers.extend([source])
      f.send_packet(packet, socket)
      packet = f.recieve_packet(socket)
      index.update(list(packet.peers))
    finally:
      socket.close()
  
  try:
    # start worker server.
    server = TCPServer((args.host, args.port), WorkerHandler)
    print 'worker: listen         ' + '(' + args.host + ', ' + str(args.port) + ')'
    server.serve_forever()
  except Exception:
    print('worker: terminate ungracefully')
  finally:
    print('worker: terminate gracefully')
    # shutdown status indicators.
    f.change_status(active=False)
    g.cleanup()
