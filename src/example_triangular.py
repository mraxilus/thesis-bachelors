#!/usr/bin/env python

import communication as c

def main():
  data = dict(zip(range(100), range(100)))

  results = c.sumbit(data, mapfn, reducefn, 'localhost', '8889')
  for result in results:
    print result


def mapfn(key, value):
  result = []
  for n in range(value + 1):
    result.append((key, n))


def reducefn(key, value):
  result = (key, sum(value))

if __name__ == '__main__':
  main()
