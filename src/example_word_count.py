#!/usr/bin/env python

import glob
import communication as c

def main():
  paths = glob.glob('book/alice_in_wonderland/*.txt')
  data = dict(('"""' + path + '"""', '"""' + get_contents(path) + '"""') for path in paths)

  results = c.sumbit(data, mapfn, reducefn, '192.168.0.12', '9000')
  for result in results:
    print result


def get_contents(path):
    handle = open(path)
    try:
        return handle.read()
    finally:
        handle.close()


def mapfn(key, value): 
  result = []
  for line in value.splitlines():
    for word in line.split():
      result.append(('"' + word.lower()[0] + '"', 1))


def reducefn(key, value):
  result = (key, len(value))


if __name__ == '__main__':
  main()
