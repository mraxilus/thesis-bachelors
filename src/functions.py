#!/usr/bin/env python

from communication_pb2 import Packet
from communication_pb2 import Task
try:
  import RPi.GPIO as GPIO
except ImportError:
  pass
import socket as Socket
import struct
import uuid
import inspect


def change_status(color='green', active=True):
  if active:
    # invert true/false due to common cathode (apparently it's opposite day).
    if color == 'red':
      GPIO.output(12, False)
      GPIO.output(16, True)
      GPIO.output(18, True)
    elif color == 'green':
      GPIO.output(12, True)
      GPIO.output(16, False)
      GPIO.output(18, True)
    elif color == 'blue':
      GPIO.output(12, True)
      GPIO.output(16, True)
      GPIO.output(18, False)
  else:
    GPIO.output(12, True)
    GPIO.output(16, True)
    GPIO.output(18, True)


def convert_function_to_string(function):
  source = inspect.getsource(function)
  source = source.splitlines()[1:]
  source = [line[2:] for line in source]
  source = "\n".join(source)
  return source


def create_task(job='', key='0', value='0', function='result = [(0, 0)]', task_type=Task.NONE):
  task = Task()
  task.job = str(job)
  task.key = str(key)
  task.value = str(value)
  task.function = str(function)
  task.type = task_type
  return task


def create_packet(source='localhost:8888', task=create_task(), peers=[]):
  packet = Packet()
  packet.source = str(source)
  packet.task.CopyFrom(task)
  packet.peers.extend(peers)
  return packet


def print_packet(packet, prefix=''):
  """
  print contents of packet.
  """
  def cap(s, l):
        return s if len(s)<=l else s[0:l-3]+'...'

  types = {Task.MAP: 'map    ', Task.REDUCE: 'reduce ', Task.NONE: 'none   '}
  if packet.task.type == Task.NONE:
    key, value = str(packet.source).split(':')
  else:
    key = str(packet.task.key)
    value = str(packet.task.value)
  print prefix + types[packet.task.type] + '(' + cap(key, 40).rstrip('\n') + ', ' + cap(value, 40).rstrip('\n') + ')'



def read_bytes(request, size):
  """
  read 'size' amount of bytes from request stream.
  """
  result = ''
  while size > 0:
    data = request.recv(size)
    result += data
    size -= len(data)
  return result


def recieve_packet(socket):
  """
  recieve protobuf packet from stream.
  """
  length = unpack_length(socket)
  data = read_bytes(socket, length)
  packet = Packet()
  packet.ParseFromString(data)
  return packet


def send_packet(packet, socket=None, host='localhost', port='8888'):
  """
  send protobuf packet to stream.
  """
  def pack_length(length):
    return struct.pack('>L', length)

  if socket is not None:
    data = packet.SerializeToString()
    data_length = pack_length(len(data))
    socket.sendall(data_length + data)
  else:
    socket = Socket.socket(Socket.AF_INET, Socket.SOCK_STREAM)
    try:
      socket.connect((host, int(port)))
      send_packet(packet, socket)
    finally:
      socket.close()


def unpack_length(socket):
    return struct.unpack('>L', read_bytes(socket, 4))[0]
